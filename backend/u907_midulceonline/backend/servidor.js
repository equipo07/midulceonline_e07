const express = require('express');
const app = express();

// establesco la conexion
const miconexion = require('./conexion');



// importo el body-parser
const bodyParser = require('body-parser');
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({extended:true}));


// importo las rutas
const rutas = require('./routers/routers');
app.use('/api',rutas);

//peticion de prueba con metodo GET
app.get('/', (req, res) => {
    res.send("servidor backend corriendo oook")
})

//INICIALIZAR SERVIDOR EN PUERTO 5000
app.listen(5000,function()
{
    console.log("mi servidor funciona en el puerto 5000 - http://localhost:5000");
})

