const mongoose = require('mongoose');
const miesquema = mongoose.Schema;
const carritoEsquema = mongoose.Schema({

    id : String,
    usuario : String,
    fecha : String,
    productos : String,
    cantidad : Number
});

const modeloCarrito = mongoose.model('carritos',carritoEsquema);

module.exports = modeloCarrito;