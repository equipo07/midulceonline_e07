const mongoose = require('mongoose');
const miesquema = mongoose.Schema;
const esquemaUsuario = mongoose.Schema({
    id: String,
    email: String,
    usuario: String,
    password: String,
    token: String,
    rol: String,
    activo: Boolean
});

const modeloUsuario = mongoose.model('usuarios',esquemaUsuario);
module.exports = modeloUsuario;