const mongoose = require('mongoose');
const miesquema = mongoose.Schema;
const esquemaFactura = mongoose.Schema({
    id: String,
    carrito: String,
    cliente: String,
    fecha: String,
    iva: Number,
    costo_total: Number
    
});

const modeloFactura = mongoose.model('facturas',esquemaFactura);
module.exports = modeloFactura;
