const mongoose = require('mongoose');
const miesquema = mongoose.Schema;
const productoEsquema = mongoose.Schema({
    id: String,
    nombre: String,
    categoria: String,
    descripcion: String,
    precio: Number,
    activo: Boolean,
});

const modeloProductos = mongoose.model('productos',productoEsquema);

module.exports = modeloProductos;