const express = require('express');
const router = express.Router();
const controladorRoles = require('../controllers/controller_roles');

router.get("/listar",controladorRoles);
router.get("/cargar/:id",controladorRoles);
router.post("/agregar",controladorRoles);
router.post("/editar/:id",controladorRoles);
router.delete("/borrar/:id",controladorRoles);

module.exports = router;