const express = require('express');
const router = express.Router();
const controladorUsuarios = require('../controllers/controller_usuarios');

router.get("/listar",controladorUsuarios);
router.get("/cargar/:id",controladorUsuarios);
router.post("/agregar",controladorUsuarios);
router.post("/editar/:id",controladorUsuarios);
router.delete("/borrar/:id",controladorUsuarios);

module.exports = router;