const express = require('express');
const router = express.Router();
const controladorCarritos = require('../controllers/controller_carritos');

router.get("/listar",controladorCarritos);
router.get("/cargar/:id",controladorCarritos);
router.post("/agregar",controladorCarritos);
router.post("/editar/:id",controladorCarritos);
router.delete("/borrar/:id",controladorCarritos);

module.exports = router;