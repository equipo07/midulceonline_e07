const modeloCategorias = require('../models/model_categorias');
const modeloProductos = require('../models/model_productos');

const miconexion = require('../conexion');

/*
//RELACION ENTRE COLECCIONES UTILIZANDO EL METODO AGGREGATE DE MONGODB
modeloClientes.aggregate([
    {
        $lookup: {
            localField: "id",
            from: "pedidos",
            foreignField: "id_cliente",
            as: "pedidos_clientes",
        },
    },
    { $unwind: "$pedidos_clientes" }])
    .then((result)=>{console.log(result)})
    .catch((error)=>{console.log(error)});
*/

//RELACION ENTRE COLECCIONES UTILIZANDO METODOS BASICOS DE MONGODB Y ARREGLOS
var dataCategorias = [];
modeloCategorias.find({id: "3"}).then(data => {
    console.log("categoria del producto:");
    console.log(data);
    data.map((d, k) => {dataCategorias.push(d.id);})
    modeloProductos.find({categoria: { $in: dataCategorias}})
    .then(data => {
        console.log("datos del producto:");
        console.log(data);
    })
    .catch((error)=>{console.log(error)});
});