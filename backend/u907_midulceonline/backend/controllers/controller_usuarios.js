const express = require('express');
const router = express.Router();

const modeloUsuario = require('../models/model_usuarios');

router.get('/listar',(req, res) => {
    modeloUsuario.find({}, function(docs,err)
    {
        if(!err)
        {
            res.send(docs);
        }
        else
        {
            res.send(err);
        }
    })
});


router.get('/cargar/:id',(req, res) => {
    modeloUsuario.find({id:req.params.id}, function(docs,err)
    {
        if(!err)
        {
            res.send(docs);
        }
        else
        {
            res.send(err);
        }
    })
});

router.post('/agregar',(req, res) =>{
    const nuevoUsuario = new modeloUsuario({
        id: req.body.id,
        email: req.body.email,
        usuario: req.body.usuario,
        password: req.body.password,
        token: req.body.token,
        rol: req.body.rol,
        activo: req.body.activo
    });

    nuevoUsuario.save(function(err)
    {
        if(!err)
        {
            res.send("EL registro se agrego exitosamente");
        }
        else
        {
            res.send(err.stack);
        }
    });
});

router.post('/editar/:id',(req,res)=>{
    modeloUsuario.findOneAndUpdate({id:req.params.id},
        {
        id: req.body.id,
        email: req.body.email,
        usuario: req.body.usuario,
        password: req.body.password,
        token: req.body.token,
        rol: req.body.rol,
        activo: req.body.activo
        },(err)=>
        {
            if(!err)
            {
                res.send("EL registro se edito exitosamente");
            }
            else
            {
                res.send(err.stack);
            }
        })

});

router.delete('/borrar/:id',(req,res)=>{
    modeloUsuario.findOneAndDelete({id:req.params.id},
        (err) =>
        {
            if(!err)
            {
                res.send("EL registro se elimino exitosamente");
            }
            else
            {
                res.send(err.stack);
            }
        })

});



module.exports = router;