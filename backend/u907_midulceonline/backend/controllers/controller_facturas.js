const express = require('express');
const router = express.Router();

const modeloFactura = require('../models/model_facturas');

router.get('/listar',(req, res) => {
    modeloFactura.find({}, function(docs,err)
    {
        if(!err)
        {
            res.send(docs);
        }
        else
        {
            res.send(err);
        }
    })
})


router.get('/cargar/:id',(req, res) => {
    modeloFactura.find({id:req.params.id}, function(docs,err)
    {
        if(!err)
        {
            res.send(docs);
        }
        else
        {
            res.send(err);
        }
    })
})

router.post('/agregar',(req, res) =>{
    const nuevoFactura = new modeloFactura({
        id: req.body.id,
        carrito: req.body.carrito,
        cliente: req.body.cliente,
        fecha: req.body.fecha,
        iva: req.body.iva,
        costo_total: req.body.costo_total
    });

    nuevoFactura.save(function(err)
    {
        if(!err)
        {
            res.send("EL registro se agrego exitosamente");
        }
        else
        {
            res.send(err.stack);
        }
    });
});

router.post('/editar/:id',(req,res)=>{
    modeloFactura.findOneAndUpdate({id:req.params.id},
        {
        id: req.body.id,
        carrito: req.body.carrito,
        cliente: req.body.cliente,
        fecha: req.body.fecha,
        iva: req.body.iva,
        costo_total: req.body.costo_total
        },(err)=>
        {
            if(!err)
            {
                res.send("EL registro se edito exitosamente");
            }
            else
            {
                res.send(err.stack);
            }
        })

});

router.delete('/borrar/:id',(req,res)=>{
    modeloFactura.findOneAndDelete({id:req.params.id},
        (err) =>
        {
            if(!err)
            {
                res.send("EL registro se elimino exitosamente");
            }
            else
            {
                res.send(err.stack);
            }
        })

});



module.exports = router;