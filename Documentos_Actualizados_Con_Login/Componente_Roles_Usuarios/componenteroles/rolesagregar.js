import axios from 'axios';
import uniquid from 'uniqid';
import React, {useEffect, useState} from 'react';
import {useParams, useNavigate} from 'react-router';
import Swal from 'sweetalert2';

function RolesAgregar()
{
    const[nombre, setNombre] = useState('')
    const[descripcion, setDescripcion] = useState('')
    const[activo, setActivo] = useState('') 
    const navigate = useNavigate()
    
    function rolesInsertar()
    {

        const rolinsertar = {
            id: uniquid(),
            nombre: nombre,
            descripcion: descripcion,
            activo: activo
        }

        console.log(rolinsertar)

        axios.post(`/api/roles/agregar`,rolinsertar).then(res => {
            console.log(res.data)
            Swal.fire({ position: 'center', icon: 'success',  title: 'El registro fue agregado exitosamente!', showConfirmButton: false, timer: 1500 })
            navigate('/roleslistar')
            })
            .catch(err => {console.log(err.stack)})
    
    }

    function rolesRegresar()
    {
        //window.location.href="/";
        navigate('/roleslistar')
    }

    return(
        <div className="container mt-5">
            <h4>Nuevo Rol</h4>
            <div className="row">
                <div className="col-md-12">
                <div className="mb-3">
                        <label htmlFor="nombre" className="form-label">Nombre</label>
                        <input type="text" className="form-control" id="nombre" value={nombre} onChange={(e) => {setNombre(e.target.value)}}></input>
                    </div>                    
                    <div className="mb-3">
                        <label htmlFor="descripcion" className="form-label">Descripcion</label>
                        <input type="text" className="form-control" id="descripcion" value={descripcion} onChange={(e) => {setDescripcion(e.target.value)}}></input>
                    </div>
                    <div className="mb-3">
                        <label htmlFor="activo" className="form-label">Activo</label>
                        <input type="text" className="form-control" id="activo" value={activo} onChange={(e) => {setActivo(e.target.value)}}></input>
                    </div>                
                    <div className="mb-12">
                        <button type="button" onClick={rolesRegresar} className="btn btn-primary">Atras</button>
                        <button type="button" onClick={rolesInsertar} className="btn btn-success">Agregar</button>
                    </div>
                </div>
            </div>
        </div>
    )

}

export default RolesAgregar;