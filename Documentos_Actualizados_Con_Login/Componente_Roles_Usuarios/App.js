//import logo from './logo.svg';
import './App.css';

import PaginaPrincipal from './componentes/paginaprincipal';

import ProductosListar from './componentes/componenteproductos/productoslistar';
import ProductosEditar from './componentes/componenteproductos/productoseditar';
import ProductosBorrar from './componentes/componenteproductos/productosborrar';
import ProductosAgregar from './componentes/componenteproductos/productosagregar';

import UsuariosListar from './componentes/componenteusuarios/usuarioslistar';
import UsuariosEditar from './componentes/componenteusuarios/usuarioseditar';
import UsuariosBorrar from './componentes/componenteusuarios/usuariosborrar';
import UsuariosAgregar from './componentes/componenteusuarios/usuariosagregar';

import RolesListar from './componentes/componenteroles/roleslistar';
import RolesEditar from './componentes/componenteroles/roleseditar';
import RolesBorrar from './componentes/componenteroles/rolesborrar';
import RolesAgregar from './componentes/componenteroles/rolesagregar';

import {BrowserRouter, Routes, Route} from 'react-router-dom';


function App() {
  return (
    <div className="App">
    <nav className="navbar navbar-expand-lg bg-light">
      <div className="container-fluid">
        <a className="navbar-brand" href="/">Mi Dulce Online</a>
        <button className="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarNav" aria-controls="navbarNav" aria-expanded="false" aria-label="Toggle navigation">
          <span className="navbar-toggler-icon"></span>
        </button>
        <div className="collapse navbar-collapse" id="navbarNav">
          <ul className="navbar-nav">
            <li className="nav-item">
              <a className="nav-link active" aria-current="page" href="/">Inicio</a>
            </li>
            <li className="nav-item">
              <a className="nav-link" href="/clienteslistar">Clientes</a>
            </li>
            <li className="nav-item">
              <a className="nav-link" href="/productoslistar">Productos</a>
            </li>
            <li className="nav-item">
              <a className="nav-link" href="/carritoslistar">Carrito</a>
            </li>
            <li className="nav-item">
            <a className="nav-link" href="/categoriaslistar">Categorias</a>
            </li>
            <li className="nav-item">
            <a className="nav-link" href="/usuarioslistar">Usuarios</a>
            </li>
            <li className="nav-item">
            <a className="nav-link" href="/roleslistar">Rol</a>
            </li>
            <li className="nav-item">
            <a className="nav-link" href="/facturaslistar">Facturas</a>
            </li>
          </ul>
        </div>
      </div>
    </nav>
    <BrowserRouter>
      <Routes>
        <Route path='/' element={<PaginaPrincipal/>} exact></Route>
        <Route path='/roleslistar' element={<RolesListar/>} exact></Route>
        <Route path='/rolesborrar/:id' element={<RolesBorrar/>} exact></Route>
        <Route path='/roleseditar/:id' element={<RolesEditar/>} exact></Route>
        <Route path='/rolesagregar' element={<RolesAgregar/>} exact></Route> 

        <Route path='/' element={<PaginaPrincipal/>} exact></Route>
        <Route path='/productoslistar' element={<ProductosListar/>} exact></Route>
        <Route path='/productosborrar/:id' element={<ProductosBorrar/>} exact></Route>
        <Route path='/productoseditar/:id' element={<ProductosEditar/>} exact></Route>
        <Route path='/productosagregar' element={<ProductosAgregar/>} exact></Route> 

        <Route path='/' element={<PaginaPrincipal/>} exact></Route>
        <Route path='/usuarioslistar' element={<UsuariosListar/>} exact></Route>
        <Route path='/usuariosborrar/:id' element={<UsuariosBorrar/>} exact></Route>
        <Route path='/usuarioseditar/:id' element={<UsuariosEditar/>} exact></Route>
        <Route path='/usuariosagregar' element={<UsuariosAgregar/>} exact></Route>     
      </Routes>    
    </BrowserRouter>
    <section className="mt-5 mb-5">
      <div align="center">
          Copyright (c) 2022 - MisionTIC - Grupo U9 - 07
      </div> 
    </section>
    </div>
  );
}

export default App;
