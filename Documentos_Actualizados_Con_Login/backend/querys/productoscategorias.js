const modeloCategoria = require('../models/model_categorias');
const modeloProductos = require('../models/model_productos');

const miconexion = require('../conexion');

/*
//RELACION ENTRE COLECCIONES UTILIZANDO EL METODO AGGREGATE DE MONGODB
modeloClientes.aggregate([
    {
        $lookup: {
            localField: "id",
            from: "pedidos",
            foreignField: "id_cliente",
            as: "pedidos_clientes",
        },
    },
    { $unwind: "$pedidos_clientes" }])
    .then((result)=>{console.log(result)})
    .catch((error)=>{console.log(error)});
*/

//RELACION ENTRE COLECCIONES UTILIZANDO METODOS BASICOS DE MONGODB Y ARREGLOS
var dataCategoria = [];
modeloCategoria.find({id: "1"}).then(data => {
    console.log("Categoria del producto:");
    console.log(data);
    data.map((d, k) => {dataCategoria.push(d.id);})     
    modeloProductos.find({categoria: { $in: dataCategoria}})
    .then(data => {
        console.log("Datos del producto:");
        console.log(data);
    })
    .catch((error)=>{console.log(error)});
});