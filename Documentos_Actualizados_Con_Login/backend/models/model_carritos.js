const mongoose = require('mongoose');
const miesquema = mongoose.Schema;
const carritoEsquema = mongoose.Schema({

    id: String,
    cliente: String,
    fecha: String,
    nombre: String,
    cantidad: Number,
});

const modeloCarrito = mongoose.model('carritos',carritoEsquema);
module.exports = modeloCarrito;
