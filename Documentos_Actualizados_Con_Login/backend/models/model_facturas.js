const mongoose = require('mongoose');
const miesquema = mongoose.Schema;
const facturaEsquema = mongoose.Schema({
    id: String,
    carrito: String,
    cliente: String,
    fecha: String,
    iva: Number,
    costo_total: Number
});

const modeloFactura = mongoose.model('facturas',facturaEsquema);
module.exports = modeloFactura;
