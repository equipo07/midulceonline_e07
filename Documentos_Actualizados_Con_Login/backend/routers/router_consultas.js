const express = require('express');
const router = express.Router();

const controladorProductosCategorias = require('../controllers/cotroller_productoscategorias');
router.get("/productoscategorias",controladorProductosCategorias);
router.get("/productoscategorias/:id",controladorProductosCategorias);

const controladorClientesFacturas = require('../controllers/controller_clientesfacturas');
router.get("/clientesfacturas",controladorClientesFacturas);
router.get("/clientesfacturas/:id",controladorClientesFacturas);

const controladorProductosCarritos = require('../controllers/controller_productoscarritos');
router.get("/productoscarritos",controladorProductosCarritos);
router.get("/productoscarritos/:id",controladorProductosCarritos);


module.exports = router;