const express = require('express');
const router = express.Router();
const controladorCarritos = require('../controllers/controller_carritos');
const auth = require('../middleware/auth');

router.get("/listar",auth,controladorCarritos);
router.get("/cargar/:id",auth,controladorCarritos);
router.post("/agregar",auth,controladorCarritos);
router.post("/editar/:id",auth,controladorCarritos);
router.delete("/borrar/:id",auth,controladorCarritos);

module.exports = router;

