const express = require('express');
const router = express.Router();
const controladorCategorias = require('../controllers/controller_categorias');
const auth = require('../middleware/auth');

router.get("/listar",auth,controladorCategorias);
router.get("/cargar/:id",auth,controladorCategorias);
router.post("/agregar",auth,controladorCategorias);
router.post("/editar/:id",auth,controladorCategorias);
router.delete("/borrar/:id",auth,controladorCategorias);

module.exports = router;