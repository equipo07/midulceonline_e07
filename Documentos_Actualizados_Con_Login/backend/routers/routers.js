const express = require('express');
const router = express.Router();

const rutaClientes = require('./router_clientes');
router.use("/clientes",rutaClientes);

const rutaCategorias = require('./router_categorias');
router.use("/categorias",rutaCategorias);

const rutaProductos = require('./router_productos');
router.use("/productos",rutaProductos);

const rutaUsuarios = require('./router_usuarios');
router.use("/usuarios",rutaUsuarios);

const rutaFacturas = require('./router_facturas');
router.use("/facturas",rutaFacturas);

const rutaRoles = require('./router_roles');
router.use("/roles",rutaRoles);

const rutaCarritos = require('./router_carritos');
router.use("/carritos",rutaCarritos);

const rutaConsultas = require('./router_consultas');
router.use("/consultas",rutaConsultas);

const rutaAuth = require('./router_auth');
router.use("/auth",rutaAuth);

module.exports = router;