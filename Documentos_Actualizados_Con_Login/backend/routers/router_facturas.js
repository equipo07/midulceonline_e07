const express = require('express');
const router = express.Router();
const controladorFacturas = require('../controllers/controller_facturas');
const auth = require('../middleware/auth');

router.get("/listar",auth,controladorFacturas);
router.get("/cargar/:id",auth,controladorFacturas);
router.post("/agregar",auth,controladorFacturas);
router.post("/editar/:id",auth,controladorFacturas);
router.delete("/borrar/:id",auth,controladorFacturas);

module.exports = router;