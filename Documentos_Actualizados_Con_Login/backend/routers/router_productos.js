const express = require('express');
const router = express.Router();
const controladorProductos = require('../controllers/controller_productos');
const auth = require('../middleware/auth');

router.get("/listar",auth,controladorProductos);
router.get("/cargar/:id",auth,controladorProductos);
router.post("/agregar",auth,controladorProductos);
router.post("/editar/:id",auth,controladorProductos);
router.delete("/borrar/:id",auth,controladorProductos);

module.exports = router;