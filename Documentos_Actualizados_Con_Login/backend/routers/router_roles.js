const express = require('express');
const router = express.Router();
const controladorRoles = require('../controllers/controller_roles');
const auth = require('../middleware/auth');

router.get("/listar",auth,controladorRoles);
router.get("/cargar/:id",auth,controladorRoles);
router.post("/agregar",auth,controladorRoles);
router.post("/editar/:id",auth,controladorRoles);
router.delete("/borrar/:id",auth,controladorRoles);

module.exports = router;