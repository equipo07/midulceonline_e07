const express = require('express');
const router = express.Router();

const modeloCarrito = require('../models/model_carritos');

router.get('/listar',(req, res) => {
    modeloCarrito.find({}, function(docs,err)
    {
        if(!err)
        {
            res.send(docs);
        }
        else
        {
            res.send(err);
        }
    })
})


router.get('/cargar/:id',(req, res) => {
    modeloCarrito.find({id:req.params.id}, function(docs,err)
    {
        if(!err)
        {
            res.send(docs);
        }
        else
        {
            res.send(err);
        }
    })
})

router.post('/agregar',(req, res) =>{
    const nuevoCarrito = new modeloCarrito({
        id: req.body.id,
        cliente: req.body.cliente,
        fecha: req.body.fecha,
        nombre: req.body.nombre,
        cantidad: req.body.cantidad
        
    });

    nuevoCarrito.save(function(err)
    {
        if(!err)
        {
            res.send("EL registro se agrego exitosamente");
        }
        else
        {
            res.send(err.stack);
        }
    });
});

router.post('/editar/:id',(req,res)=>{
    modeloCarrito.findOneAndUpdate({id:req.params.id},
        {
        id: req.body.id,
        cliente: req.body.cliente,
        fecha: req.body.fecha,
        nombre: req.body.nombre,
        cantidad: req.body.cantidad       
        },(err)=>
        {
            if(!err)
            {
                res.send("EL registro se edito exitosamente");
            }
            else
            {
                res.send(err.stack);
            }
        })

});

router.delete('/borrar/:id',(req,res)=>{
    modeloCarrito.findOneAndDelete({id:req.params.id},
        (err) =>
        {
            if(!err)
            {
                res.send("EL registro se elimino exitosamente");
            }
            else
            {
                res.send(err.stack);
            }
        })

});

module.exports = router;