const express = require('express');
const router = express.Router();
const modeloProductos = require('../models/model_productos');
const modeloCategorias = require('../models/model_categorias');

router.get('/productoscategorias', (req, res) => {
    modeloCategorias.aggregate([
        {
            $lookup: {
                localField: "id",
                from: "productos",
                foreignField: "categoria",
                as: "productos de la categoria",
            },
        },
        { $unwind: "$productos de la categoria" }])
        .then((result)=>{res.send(result); console.log(result);})
        .catch((error)=>{res.send(error); console.log(error);});
});

router.get('/productoscategorias/:id', (req, res) => {
    var dataCategoria = [];
    modeloCategorias.find({id:req.params.id}).then(data => {
        console.log("Categoria del Producto:");
        console.log(data);
        data.map((d, k) => {dataCategoria.push(d.id);})
        modeloProductos.find({categoria: { $in: dataCategoria}})
        .then(data => {
            console.log("Datos del Producto");
            console.log(data);
            res.send(data);
        })
        .catch((error)=>{res.send(error); console.log(error);});
    });
});

module.exports = router;