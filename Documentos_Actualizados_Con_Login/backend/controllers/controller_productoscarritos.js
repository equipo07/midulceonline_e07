const express = require('express');
const router = express.Router();
const modeloProductos = require('../models/model_productos');
const modeloCarritos = require('../models/model_carritos');

router.get('/productoscarritos', (req, res) => {
    modeloCarritos.aggregate([
        {
            $lookup: {
                localField: "id",
                from: "productos",
                foreignField: "id",
                as: "productos del carrito",
            },
        },
        { $unwind: "$productos del carrito" }])
        .then((result)=>{res.send(result); console.log(result);})
        .catch((error)=>{res.send(error); console.log(error);});
});

router.get('/productoscarritos/:id', (req, res) => {
    var dataCarrito = [];
    modeloCarritos.find({id:req.params.id}).then(data => {
        console.log("Informacion del Carrito:");
        console.log(data);
        data.map((d, k) => {dataCarrito.push(d.id);})
        modeloProductos.find({id: { $in: dataCarrito}})
        .then(data => {
            console.log("Datos del Producto");
            console.log(data);
            res.send(data);
        })
        .catch((error)=>{res.send(error); console.log(error);});
    });
});

module.exports = router;