const express = require('express');
const router = express.Router();

const modeloCategoria = require('../models/model_categorias');

router.get('/listar',(req, res) => {
    modeloCategoria.find({}, function(docs,err)
    {
        if(!err)
        {
            res.send(docs);
        }
        else
        {
            res.send(err);
        }
    })
})


router.get('/cargar/:id',(req, res) => {
    modeloCategoria.find({id:req.params.id}, function(docs,err)
    {
        if(!err)
        {
            res.send(docs);
        }
        else
        {
            res.send(err);
        }
    })
})

router.post('/agregar',(req, res) =>{
    const nuevoCategoria = new modeloCategoria({
        id: req.body.id,
        nombre: req.body.nombre,
        activo: req.body.activo
    });

    nuevoCategoria.save(function(err)
    {
        if(!err)
        {
            res.send("EL registro se agrego exitosamente");
        }
        else
        {
            res.send(err.stack);
        }
    });
});

router.post('/editar/:id',(req,res)=>{
    modeloCategoria.findOneAndUpdate({id:req.params.id},
        {
        id: req.body.id,
        nombre: req.body.nombre,
        activo: req.body.activo
        },(err)=>
        {
            if(!err)
            {
                res.send("EL registro se edito exitosamente");
            }
            else
            {
                res.send(err.stack);
            }
        })

});

router.delete('/borrar/:id',(req,res)=>{
    modeloCategoria.findOneAndDelete({id:req.params.id},
        (err) =>
        {
            if(!err)
            {
                res.send("EL registro se elimino exitosamente");
            }
            else
            {
                res.send(err.stack);
            }
        })

});

module.exports = router;