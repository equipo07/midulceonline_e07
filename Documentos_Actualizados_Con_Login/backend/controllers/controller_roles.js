const express = require('express');
const router = express.Router();

const modeloRol = require('../models/model_roles');

router.get('/listar',(req, res) => {
    modeloRol.find({}, function(docs,err)
    {
        if(!err)
        {
            res.send(docs);
        }
        else
        {
            res.send(err);
        }
    })
})


router.get('/cargar/:id',(req, res) => {
    modeloRol.find({id:req.params.id}, function(docs,err)
    {
        if(!err)
        {
            res.send(docs);
        }
        else
        {
            res.send(err);
        }
    })
})

router.post('/agregar',(req, res) =>{
    const nuevoRol = new modeloRol({
        id: req.body.id,
        nombre: req.body.nombre,
        descripcion: req.body.descripcion,
        activo: req.body.activo
    });

    nuevoRol.save(function(err)
    {
        if(!err)
        {
            res.send("EL registro se agrego exitosamente");
        }
        else
        {
            res.send(err.stack);
        }
    });
});

router.post('/editar/:id',(req,res)=>{
    modeloRol.findOneAndUpdate({id:req.params.id},
        {
        id: req.body.id,
        nombre: req.body.nombre,
        descripcion: req.body.descripcion,
        activo: req.body.activo
        },(err)=>
        {
            if(!err)
            {
                res.send("EL registro se edito exitosamente");
            }
            else
            {
                res.send(err.stack);
            }
        })

});

router.delete('/borrar/:id',(req,res)=>{
    modeloRol.findOneAndDelete({id:req.params.id},
        (err) =>
        {
            if(!err)
            {
                res.send("EL registro se elimino exitosamente");
            }
            else
            {
                res.send(err.stack);
            }
        })

});

module.exports = router;