const express = require('express');
const router = express.Router();
const modeloFacturas = require('../models/model_facturas');
const modeloCliente = require('../models/model_clientes');

router.get('/clientesfacturas', (req, res) => {
    modeloCliente.aggregate([
        {
            $lookup: {
                localField: "id",
                from: "facturas",
                foreignField: "cliente",
                as: "Facturas del Cliente",
            },
        },
        { $unwind: "$Facturas del Cliente" }])
        .then((result)=>{res.send(result); console.log(result);})
        .catch((error)=>{res.send(error); console.log(error);});
});

router.get('/clientesfacturas/:id', (req, res) => {
    var dataCliente = [];
    modeloCliente.find({id:req.params.id}).then(data => {
        console.log("Información del Cliente");
        console.log(data);
        data.map((d, k) => {dataCliente.push(d.id);})
        modeloFacturas.find({cliente: { $in: dataCliente}})
        .then(data => {
            console.log("Datos de la Factura");
            console.log(data);
            res.send(data);
        })
        .catch((error)=>{res.send(error); console.log(error);});
    });
});

module.exports = router;