import axios from 'axios';
import React, {useEffect, useState} from 'react';
import {Link} from 'react-router-dom';
import FacturasBorrar from './facturasborrar';

//Metodo que contiene las tareas para listar facturas
function FacturasListar()
{

    const[dataFacturas, setdataFacturas] = useState([])

    useEffect(()=>{
        axios.get('/api/facturas/listar').then(res => {
        console.log(res.data)
        setdataFacturas(res.data)
        }).catch(err=>{console.log(err.stack)})
    },[])
    
    return (
        <div className="container mt-5">
        <h1>Lista de Facturas</h1>
        <div className="row">
            <div className="col-md-12">
                <table className="table table-bordered">
                    <thead className="thead-dark">
                        <tr key={0}>
                            <td align="center">Id</td>
                            <td>Carrito</td>
                            <td>Fecha</td>
                            <td>Cliente</td>
                            <td>Iva</td>
                            <td align="center">Costo Total</td>
                            {/*<td align="center"></td>*/}
                            <td colSpan={7} align="center"><Link to={`/facturasagregar`}><li className='btn btn-success'>Agregar Factura</li></Link></td>
                        </tr>
                    </thead> 
                    <tbody className='dialog'>
                    {
                        dataFacturas.map(mifactura => (
                        <tr key={mifactura.id}>
                            <td align="center">{mifactura.id}</td>
                            <td>{mifactura.carrito}</td>
                            <td>{mifactura.fecha}</td>
                            <td>{mifactura.cliente}</td>
                            <td align="center">{mifactura.iva}</td>
                            <td align="right">{mifactura.costo_total}</td>
                            <td align="center"><Link to={`/facturaseditar/${mifactura.id}`}><li className='btn btn-info'>Editar</li></Link></td>
                            <td align="center"><li className='btn btn-danger' onClick={()=>{FacturasBorrar(mifactura.id)}}>Borrar</li></td>
                        </tr>
                    ))
                    }
                    </tbody>
                </table>
            </div>
        </div>
        </div>
    )

}
export default FacturasListar;
