import axios from 'axios';
import React, {useEffect, useState} from 'react';
import {useParams, useNavigate} from 'react-router';
import Swal from 'sweetalert2';

function ClientesEditar()
{
    const parametros = useParams()
    
    const[id_tipodocumento, setId_tipodocumento] = useState('')
    const[nombre, setNombre] = useState('')
    const[telefono, setTelefono] = useState('')
    const[direccion, setDireccion] = useState('')
    const[email, setEmail] = useState('')
    const[activo, setActivo] = useState('') 
    const navigate = useNavigate()
    
    useEffect(()=>{
        //axios.post('api/clientes/cargardata', {id: parametros.id}).then(res => {
        axios.get(`/api/clientes/cargar/${parametros.id}`).then(res => {
        //axios.post(`/api/clientes/cargardata/${parametros.id}`).then(res => {
        console.log(res.data[0])
        const dataClientes = res.data[0]
        setId_tipodocumento(dataClientes.id_tipodocumento)
        setNombre(dataClientes.nombre)
        setTelefono(dataClientes.telefono)
        setDireccion(dataClientes.direccion)
        setEmail(dataClientes.email)
        setActivo(dataClientes.activo)
        })
    }, [])

    function clientesActualizar()
    {
        const clienteactualizar = {
            id: parametros.id,
            id_tipodocumento: id_tipodocumento,
            nombre: nombre,
            telefono: telefono,
            direccion: direccion,
            email: email,
            activo: activo
        }

        console.log(clienteactualizar)

        axios.post(`/api/clientes/editar/${parametros.id}`,clienteactualizar).then(res => {
            console.log(res.data)
            Swal.fire({ position: 'center', icon: 'success',  title: 'El registro fue actualizado exitosamente!', showConfirmButton: false, timer: 1500 })
            navigate('/clienteslistar')
            })
            .catch(err => {console.log(err.stack)})
    
    }

    function clientesRegresar()
    {
        //window.location.href="/";
        navigate('/clienteslistar')
    }

    return(
        <div className="container mt-5">
            <h4>Cliente</h4>
            <div className="row">
                <div className="col-md-12">
                <div className="mb-3">
                        <label htmlFor="id_tipodocumento" className="form-label">Id_tipodocumento</label>
                        <input type="text" className="form-control" id="id_tipodocumento" value={id_tipodocumento} onChange={(e) => {setId_tipodocumento(e.target.value)}}></input>
                    </div>
                    <div className="mb-3">
                        <label htmlFor="nombre" className="form-label">Nombre</label>
                        <input type="text" className="form-control" id="nombre" value={nombre} onChange={(e) => {setNombre(e.target.value)}}></input>
                    </div>                    
                    <div className="mb-3">
                        <label htmlFor="telefono" className="form-label">Telefono</label>
                        <input type="text" className="form-control" id="telefono" value={telefono} onChange={(e) => {setTelefono(e.target.value)}}></input>
                    </div>
                    <div className="mb-3">
                        <label htmlFor="direccion" className="form-label">Direccion</label>
                        <input type="text" className="form-control" id="direccion" value={direccion} onChange={(e) => {setDireccion(e.target.value)}}></input>
                    </div>
                    <div className="mb-3">
                        <label htmlFor="email" className="form-label">Email</label>
                        <input type="text" className="form-control" id="email" value={email} onChange={(e) => {setEmail(e.target.value)}}></input>
                    </div>
                    <div className="mb-3">
                        <label htmlFor="activo" className="form-label">Activo</label>
                        <input type="text" className="form-control" id="activo" value={activo} onChange={(e) => {setActivo(e.target.value)}}></input>
                    </div>                
                    <div className="mb-12">
                        <button type="button" onClick={clientesRegresar} className="btn btn-primary">Atras</button>
                        <button type="button" onClick={clientesActualizar} className="btn btn-success">Actualizar</button>
                    </div>
                </div>
            </div>
        </div>
    )

}



export default ClientesEditar;