import axios from 'axios';
import React, {useEffect, useState} from 'react';
import {Link} from 'react-router-dom';
import CategoriasBorrar from './categoriasborrar';

//Metodo que contiene las tareas para listar pedidos
function CategoriasListar()
{

    const[dataCategorias, setdataCategorias] = useState([])

    useEffect(()=>{
        axios.get('/api/categorias/listar').then(res => {
        console.log(res.data)
        setdataCategorias(res.data)
        }).catch(err=>{console.log(err.stack)})
    },[])
    
    return (
        <div className="container mt-5">
        <h1>Lista de Categorias</h1>
        <div className="row">
            <div className="col-md-12">
                <table className="table table-bordered">
                    <thead className="thead-dark">
                        <tr key={0}>
                            <td align="center">Id</td>
                            <td>Nombre</td>
                            <td align="center">Estado</td>
                            {/*<td align="center"></td>*/}
                            <td colSpan={7} align="center"><Link to={`/categoriasagregar`}><li className='btn btn-success'>Agregar Categoria</li></Link></td>
                        </tr>
                    </thead> 
                    <tbody className='dialog'>
                    {
                        dataCategorias.map(micategoria => (
                        <tr key={micategoria.id}>
                            <td align="center">{micategoria.id}</td>
                            <td>{micategoria.nombre}</td>
                            <td align="center">{micategoria.activo ? 'Activo' : 'Inactivo'}</td>
                            <td align="center"><Link to={`/categoriaseditar/${micategoria.id}`}><li className='btn btn-info'>Editar</li></Link></td>
                            <td align="center"><li className='btn btn-danger' onClick={()=>{CategoriasBorrar(micategoria.id)}}>Borrar</li></td>
                        </tr>
                    ))
                    }
                    </tbody>
                </table>
            </div>
        </div>
        </div>
    )

}
export default CategoriasListar;

