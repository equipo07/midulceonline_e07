import axios from 'axios';
import uniquid from 'uniqid';
import React, {useEffect, useState} from 'react';
import {useParams, useNavigate} from 'react-router';
import Swal from 'sweetalert2';

function CategoriasAgregar()
{
    const[nombre, setNombre] = useState('')
    const[activo, setActivo] = useState('') 
    const navigate = useNavigate()
    
    function categoriasInsertar()
    {

        const categoriainsertar = {
            id: uniquid(),
            nombre: nombre,
            activo: activo
        }

        console.log(categoriainsertar)

        axios.post(`/api/categorias/agregar`,categoriainsertar).then(res => {
            console.log(res.data)
            Swal.fire({ position: 'center', icon: 'success',  title: 'El registro fue agregado exitosamente!', showConfirmButton: false, timer: 1500 })
            navigate('/categoriaslistar')
            })
            .catch(err => {console.log(err.stack)})
    
    }

    function categoriasRegresar()
    {
        //window.location.href="/";
        navigate('/categoriaslistar')
    }

    return(
        <div className="container mt-5">
            <h4>Nueva Categoria</h4>
            <div className="row">
                <div className="col-md-12">
                <div className="mb-3">
                        <label htmlFor="nombre" className="form-label">Nombre</label>
                        <input type="text" className="form-control" id="nombre" value={nombre} onChange={(e) => {setNombre(e.target.value)}}></input>
                    </div>                    
                    <div className="mb-3">
                        <label htmlFor="activo" className="form-label">Activo</label>
                        <input type="text" className="form-control" id="activo" value={activo} onChange={(e) => {setActivo(e.target.value)}}></input>
                    </div>                
                    <div className="mb-12">
                        <button type="button" onClick={categoriasRegresar} className="btn btn-primary">Atras</button>
                        <button type="button" onClick={categoriasInsertar} className="btn btn-success">Agregar</button>
                    </div>
                </div>
            </div>
        </div>
    )

}

export default CategoriasAgregar;