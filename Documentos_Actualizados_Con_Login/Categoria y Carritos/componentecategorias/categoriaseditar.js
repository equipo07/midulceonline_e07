import axios from 'axios';
import React, {useEffect, useState} from 'react';
import {useParams, useNavigate} from 'react-router';
import Swal from 'sweetalert2';

function CategoriasEditar()
{
    const parametros = useParams()
    
    const[nombre, setNombre] = useState('')
    const[activo, setActivo] = useState('') 
    const navigate = useNavigate()
    
    useEffect(()=>{
        //axios.post('api/pedidos/cargardata', {id: parametros.id}).then(res => {
        axios.get(`/api/categorias/cargar/${parametros.id}`).then(res => {
        //axios.post(`/api/pedidos/cargardata/${parametros.id}`).then(res => {
        console.log(res.data[0])
        const dataCategorias = res.data[0]
        setNombre(dataCategorias.nombre)
        setActivo(dataCategorias.activo)
        })
    }, [])

    function categoriasActualizar()
    {
        const categoriaactualizar = {
            id: parametros.id,
            nombre: nombre,

            activo: activo
        }

        console.log(categoriaactualizar)

        axios.post(`/api/categorias/editar/${parametros.id}`,categoriaactualizar).then(res => {
            console.log(res.data)
            Swal.fire({ position: 'center', icon: 'success',  title: 'El registro fue actualizado exitosamente!', showConfirmButton: false, timer: 1500 })
            navigate('/categoriaslistar')
            })
            .catch(err => {console.log(err.stack)})
    
    }

    function categoriasRegresar()
    {
        //window.location.href="/";
        navigate('/categoriaslistar')
    }

    return(
        <div className="container mt-5">
            <h4>Categoria</h4>
            <div className="row">
                <div className="col-md-12">
                <div className="mb-3">
                        <label htmlFor="nombre" className="form-label">Nombre</label>
                        <input type="text" className="form-control" id="nombre" value={nombre} onChange={(e) => {setNombre(e.target.value)}}></input>
                    </div>                    
                    <div className="mb-3">
                        <label htmlFor="activo" className="form-label">Activo</label>
                        <input type="text" className="form-control" id="activo" value={activo} onChange={(e) => {setActivo(e.target.value)}}></input>
                    </div>                
                    <div className="mb-12">
                        <button type="button" onClick={categoriasRegresar} className="btn btn-primary">Atras</button>
                        <button type="button" onClick={categoriasActualizar} className="btn btn-success">Actualizar</button>
                    </div>
                </div>
            </div>
        </div>
    )

}

//<div className="mb-3 form-check form-switch">
//<input className="form-check-input" type="checkbox" role="switch" id="activo"></input>
//</div>

export default CategoriasEditar;