import axios from 'axios';
import uniquid from 'uniqid';
import React, {useEffect, useState} from 'react';
import {useParams, useNavigate} from 'react-router';
import Swal from 'sweetalert2';

function CarritosAgregar()
{
    const[cliente, setCliente] = useState('')
    const[fecha, setFecha] = useState('')
    const[nombre, setNombre] = useState('')
    const[cantidad, setCantidad] = useState('') 
    const navigate = useNavigate()
    
    function carritosInsertar()
    {

        const carritoinsertar = {
            id: uniquid(),
            cliente: cliente,
            fecha: fecha,
            nombre : nombre,
            cantidad : cantidad,
        }

        console.log(carritoinsertar)

        axios.post(`/api/carritos/agregar`,carritoinsertar).then(res => {
            console.log(res.data)
            Swal.fire({ position: 'center', icon: 'success',  title: 'El registro fue agregado exitosamente!', showConfirmButton: false, timer: 1500 })
            navigate('/carritoslistar')
            })
            .catch(err => {console.log(err.stack)})
    
    }

    function carritosRegresar()
    {
        //window.location.href="/";
        navigate('/carritoslistar')
    }

    return(
        <div className="container mt-5">
            <h4>Nuevo Carrito</h4>
            <div className="row">
                <div className="col-md-12">
                <div className="mb-3">
                <label htmlFor="cliente" className="form-label">Cliente</label>
                        <input type="text" className="form-control" id="cliente" value={cliente} onChange={(e) => {setCliente(e.target.value)}}></input>
                    </div>                    
                    <div className="mb-3">
                        <label htmlFor="fecha" className="form-label">Fecha</label>
                        <input type="text" className="form-control" id="fecha" value={fecha} onChange={(e) => {setFecha(e.target.value)}}></input>
                    </div>
                    <div className="mb-3">
                        <label htmlFor="nombre" className="form-label">Nombre</label>
                        <input type="text" className="form-control" id="nombre" value={nombre} onChange={(e) => {setNombre(e.target.value)}}></input>
                    </div>
                    <div className="mb-3">
                        <label htmlFor="cantidad" className="form-label">Cantidad</label>
                        <input type="Number" className="form-control" id="cantidad" value={cantidad} onChange={(e) => {setCantidad(e.target.value)}}></input>
                    </div>                
                    <div className="mb-12">
                        <button type="button" onClick={carritosRegresar} className="btn btn-primary">Atras</button>
                        <button type="button" onClick={carritosInsertar} className="btn btn-success">Agregar</button>
                    </div>
                </div>
            </div>
        </div>
    )

}

export default CarritosAgregar;