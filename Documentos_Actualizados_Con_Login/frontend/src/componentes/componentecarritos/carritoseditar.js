import axios from 'axios';
import React, {useEffect, useState} from 'react';
import {useParams, useNavigate} from 'react-router';
import Swal from 'sweetalert2';

function CarritosEditar()
{
    const parametros = useParams()
    
    const[cliente, setCliente] = useState('')
    const[fecha, setFecha] = useState('')
    const[nombre, setNombre] = useState('')
    const[cantidad, setCantidad] = useState('') 
    const navigate = useNavigate()
    
    useEffect(()=>{
        //axios.post('api/carritos/cargardata', {id: parametros.id}).then(res => {
        axios.get(`/api/carritos/cargar/${parametros.id}`).then(res => {
        //axios.post(`/api/carritos/cargardata/${parametros.id}`).then(res => {
        console.log(res.data[0])
        const dataCarritos = res.data[0]
        setCliente(dataCarritos.cliente)
        setFecha(dataCarritos.fecha)
        setNombre(dataCarritos.nombre)
        setCantidad(dataCarritos.cantidad)
        })
    }, [])

    function carritosActualizar()
    {
        const carritoactualizar = {
            id: parametros.id,
            cliente: cliente,
            fecha: fecha,
            nombre: nombre,
            cantidad: cantidad,
        }

        console.log(carritoactualizar)

        axios.post(`/api/carritos/editar/${parametros.id}`,carritoactualizar).then(res => {
            console.log(res.data)
            Swal.fire({ position: 'center', icon: 'success',  title: 'El registro fue actualizado exitosamente!', showConfirmButton: false, timer: 1500 })
            navigate('/carritoslistar')
            })
            .catch(err => {console.log(err.stack)})
    
    }

    function carritosRegresar()
    {
        //window.location.href="/";
        navigate('/carritoslistar')
    }

    return(
        <div className="container mt-5">
            <h4>Carrito de Compras</h4>
            <div className="row">
                <div className="col-md-12">
                <div className="mb-3">
                        <label htmlFor="cliente" className="form-label">Cliente</label>
                        <input type="text" className="form-control" id="cliente" value={cliente} onChange={(e) => {setCliente(e.target.value)}}></input>
                    </div>                    
                    <div className="mb-3">
                        <label htmlFor="fecha" className="form-label">Fecha</label>
                        <input type="text" className="form-control" id="fecha" value={fecha} onChange={(e) => {setFecha(e.target.value)}}></input>
                    </div>
                    <div className="mb-3">
                        <label htmlFor="nombre" className="form-label">Nombre</label>
                        <input type="text" className="form-control" id="nombre" value={nombre} onChange={(e) => {setNombre(e.target.value)}}></input>
                    </div>
                    <div className="mb-3">
                        <label htmlFor="cantidad" className="form-label">Cantidad</label>
                        <input type="Number" className="form-control" id="cantidad" value={cantidad} onChange={(e) => {setCantidad(e.target.value)}}></input>
                    </div>
                    <div className="mb-12">
                        <button type="button" onClick={carritosRegresar} className="btn btn-primary">Atras</button>
                        <button type="button" onClick={carritosActualizar} className="btn btn-success">Actualizar</button>
                    </div>
                </div>
            </div>
        </div>
    )

}


export default CarritosEditar;