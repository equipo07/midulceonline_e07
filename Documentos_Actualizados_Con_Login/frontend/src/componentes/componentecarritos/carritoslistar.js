import axios from 'axios';
import React, {useEffect, useState} from 'react';
import {Link} from 'react-router-dom';
import CarritosBorrar from './carritosborrar';

//Metodo que contiene las tareas para listar facturas
function CarritosListar()
{

    const[dataCarritos, setdataCarritos] = useState([])

    useEffect(()=>{
        axios.get('/api/carritos/listar').then(res => {
        console.log(res.data)
        setdataCarritos(res.data)
        }).catch(err=>{console.log(err.stack)})
    },[])
    
    return (
        <div className="container mt-5">
        <h1>Lista de Carritos</h1>
        <div className="row">
            <div className="col-md-12">
                <table className="table table-bordered">
                    <thead className="thead-dark">
                        <tr key={0}>
                            <td align="center">Id</td>
                            <td>Cliente</td>
                            <td>Fecha</td>
                            <td>Nombre</td>
                            <td align="center">Cantidad</td>
                            {/*<td align="center"></td>*/}
                            <td colSpan={7} align="center"><Link to={`/carritosagregar`}><li className='btn btn-success'>Agregar Carrito</li></Link></td>
                        </tr>
                    </thead> 
                    <tbody className='dialog'>
                    {
                        dataCarritos.map(micarrito => (
                        <tr key={micarrito.id}>
                            <td align="center">{micarrito.id}</td>
                            <td>{micarrito.cliente}</td>
                            <td>{micarrito.fecha}</td>
                            <td>{micarrito.nombre}</td>
                            <td align="center">{micarrito.cantidad}</td>
                            <td align="center"><Link to={`/carritoseditar/${micarrito.id}`}><li className='btn btn-info'>Editar</li></Link></td>
                            <td align="center"><li className='btn btn-danger' onClick={()=>{CarritosBorrar(micarrito.id)}}>Borrar</li></td>
                        </tr>
                    ))
                    }
                    </tbody>
                </table>
            </div>
        </div>
        </div>
    )

}
export default CarritosListar;
