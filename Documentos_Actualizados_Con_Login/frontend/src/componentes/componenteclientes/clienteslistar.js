import axios from 'axios';
import React, {useEffect, useState} from 'react';
import {Link} from 'react-router-dom';
import ClientesBorrar from './clientesborrar';

//Metodo que contiene las tareas para listar clientes
function ClientesListar()
{

    const[dataClientes, setdataClientes] = useState([])

    useEffect(()=>{
        axios.get('/api/clientes/listar').then(res => {
        console.log(res.data)
        setdataClientes(res.data)
        }).catch(err=>{console.log(err.stack)})
    },[])
    
    return (
        <div className="container mt-5">
        <h1>Lista de Clientes</h1>
        <div className="row">
            <div className="col-md-12">
                <table className="table table-bordered">
                    <thead className="thead-dark">
                        <tr key={0}>
                            <td align="center">Id</td>
                            <td>Id_tipodocumento</td>
                            <td>Nombre</td>
                            <td>Telefono</td>
                            <td>Direccion</td>
                            <td>Email</td>
                            <td align="center">Estado</td>
                            {/*<td align="center"></td>*/}
                            <td colSpan={7} align="center"><Link to={`/clientesagregar`}><li className='btn btn-success'>Agregar Cliente</li></Link></td>
                        </tr>
                    </thead> 
                    <tbody className='dialog'>
                    {
                        dataClientes.map(micliente => (
                        <tr key={micliente.id}>
                            <td align="center">{micliente.id}</td>
                            <td>{micliente.id_tipodocumento}</td>
                            <td>{micliente.nombre}</td>
                            <td>{micliente.telefono}</td>
                            <td>{micliente.direccion}</td>
                            <td>{micliente.email}</td>
                            <td align="center">{micliente.activo ? 'Activo' : 'Inactivo'}</td>
                            <td align="center"><Link to={`/clienteseditar/${micliente.id}`}><li className='btn btn-info'>Editar</li></Link></td>
                            <td align="center"><li className='btn btn-danger' onClick={()=>{ClientesBorrar(micliente.id)}}>Borrar</li></td>
                        </tr>
                    ))
                    }
                    </tbody>
                </table>
            </div>
        </div>
        </div>
    )

}
export default ClientesListar;