import axios from 'axios';
import React, {useEffect, useState} from 'react';
import {useParams, useNavigate} from 'react-router';
import Swal from 'sweetalert2';

function RolesEditar()
{
    const parametros = useParams()
    
    const[nombre, setNombre] = useState('')
    const[descripcion, setDescripcion] = useState('')
    const[activo, setActivo] = useState('') 
    const navigate = useNavigate()
    
    useEffect(()=>{
        //axios.post('api/pedidos/cargardata', {id: parametros.id}).then(res => {
        axios.get(`/api/roles/cargar/${parametros.id}`).then(res => {
        //axios.post(`/api/pedidos/cargardata/${parametros.id}`).then(res => {
        console.log(res.data[0])
        const dataRoles = res.data[0]
        setNombre(dataRoles.nombre)
        setDescripcion(dataRoles.descripcion)
        setActivo(dataRoles.activo)
        })
    }, [])

    function rolesActualizar()
    {
        const rolactualizar = {
            id: parametros.id,
            nombre: nombre,
            descripcion: descripcion,
            activo: activo
        }

        console.log(rolactualizar)

        axios.post(`/api/roles/editar/${parametros.id}`,rolactualizar).then(res => {
            console.log(res.data)
            Swal.fire({ position: 'center', icon: 'success',  title: 'El registro fue actualizado exitosamente!', showConfirmButton: false, timer: 1500 })
            navigate('/roleslistar')
            })
            .catch(err => {console.log(err.stack)})
    
    }

    function rolesRegresar()
    {
        //window.location.href="/";
        navigate('/roleslistar')
    }

    return(
        <div className="container mt-5">
            <h4>Rol</h4>
            <div className="row">
                <div className="col-md-12">
                <div className="mb-3">
                        <label htmlFor="nombre" className="form-label">Nombre</label>
                        <input type="text" className="form-control" id="nombre" value={nombre} onChange={(e) => {setNombre(e.target.value)}}></input>
                    </div>                    
                    <div className="mb-3">
                        <label htmlFor="descripcion" className="form-label">Descripcion</label>
                        <input type="text" className="form-control" id="descripcion" value={descripcion} onChange={(e) => {setDescripcion(e.target.value)}}></input>
                    </div>
                    <div className="mb-3">
                        <label htmlFor="activo" className="form-label">Activo</label>
                        <input type="text" className="form-control" id="activo" value={activo} onChange={(e) => {setActivo(e.target.value)}}></input>
                    </div>                
                    <div className="mb-12">
                        <button type="button" onClick={rolesRegresar} className="btn btn-primary">Atras</button>
                        <button type="button" onClick={rolesActualizar} className="btn btn-success">Actualizar</button>
                    </div>
                </div>
            </div>
        </div>
    )

}

//<div className="mb-3 form-check form-switch">
//<input className="form-check-input" type="checkbox" role="switch" id="activo"></input>
//</div>

export default RolesEditar;