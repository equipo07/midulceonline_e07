import axios from 'axios';
import React, {useEffect, useState} from 'react';
import {Link} from 'react-router-dom';
import RolesBorrar from './rolesborrar';

//Metodo que contiene las tareas para listar pedidos
function RolesListar()
{

    const[dataRoles, setdataRoles] = useState([])

    useEffect(()=>{
        axios.get('/api/roles/listar').then(res => {
        console.log(res.data)
        setdataRoles(res.data)
        }).catch(err=>{console.log(err.stack)})
    },[])
    
    return (
        <div className="container mt-5">
        <h1>Lista de Roles</h1>
        <div className="row">
            <div className="col-md-12">
                <table className="table table-bordered">
                    <thead className="thead-dark">
                        <tr key={0}>
                            <td align="center">Id</td>
                            <td>Nombre</td>
                            <td>Descripcion</td>
                            <td align="center">Estado</td>
                            {/*<td align="center"></td>*/}
                            <td colSpan={7} align="center"><Link to={`/rolesagregar`}><li className='btn btn-success'>Agregar Rol</li></Link></td>
                        </tr>
                    </thead> 
                    <tbody className='dialog'>
                    {
                        dataRoles.map(mirol => (
                        <tr key={mirol.id}>
                            <td align="center">{mirol.id}</td>
                            <td>{mirol.nombre}</td>
                            <td>{mirol.descripcion}</td>
                            <td align="center">{mirol.activo ? 'Activo' : 'Inactivo'}</td>
                            <td align="center"><Link to={`/roleseditar/${mirol.id}`}><li className='btn btn-info'>Editar</li></Link></td>
                            <td align="center"><li className='btn btn-danger' onClick={()=>{RolesBorrar(mirol.id)}}>Borrar</li></td>
                        </tr>
                    ))
                    }
                    </tbody>
                </table>
            </div>
        </div>
        </div>
    )

}
export default RolesListar;

