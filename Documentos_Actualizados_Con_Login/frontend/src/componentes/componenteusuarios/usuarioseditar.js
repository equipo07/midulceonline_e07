import axios from 'axios';
import React, {useEffect, useState} from 'react';
import {useParams, useNavigate} from 'react-router';
import Swal from 'sweetalert2';

function UsuariosEditar()
{
    const parametros = useParams()
    
    const[correo, setCorreo] = useState('')
    const[usuario, setUsuario] = useState('')
    const[password, setPassword] = useState('')
    const[token, setToken] = useState('')
    const[roles, setRoles] = useState('')
    const[activo, setActivo] = useState('')
    const navigate = useNavigate()
    
    useEffect(()=>{
        //axios.post('api/pedidos/cargardata', {id: parametros.id}).then(res => {
        axios.get(`/api/usuarios/cargar/${parametros.id}`).then(res => {
        //axios.post(`/api/pedidos/cargardata/${parametros.id}`).then(res => {
        console.log(res.data[0])
        const dataUsuarios = res.data[0]
        setCorreo(dataUsuarios.correo)
        setUsuario(dataUsuarios.usuario)
        setPassword(dataUsuarios.password)
        setToken(dataUsuarios.token)
        setRoles(dataUsuarios.roles)
        setActivo(dataUsuarios.activo)
        })
    }, [])

    function usuariosActualizar()
    {
        const usuarioactualizar = {
            id: parametros.id,
            correo: correo,
            usuario: usuario,
            password: password,
            token: token,
            roles: roles,
            activo: activo
        }

        console.log(usuarioactualizar)

        axios.post(`/api/usuarios/editar/${parametros.id}`,usuarioactualizar).then(res => {
            console.log(res.data)
            Swal.fire({ position: 'center', icon: 'success',  title: 'El registro fue actualizado exitosamente!', showConfirmButton: false, timer: 1500 })
            navigate('/usuarioslistar')
            })
            .catch(err => {console.log(err.stack)})
    
    }

    function usuariosRegresar()
    {
        //window.location.href="/";
        navigate('/usuarioslistar')
    }

    return(
        <div className="container mt-5">
            <h4>Usuario</h4>
            <div className="row">
                <div className="col-md-12">
                <div className="mb-3">
                        <label htmlFor="correo" className="form-label">Correo</label>
                        <input type="text" className="form-control" id="correo" value={correo} onChange={(e) => {setCorreo(e.target.value)}}></input>
                    </div>                    
                    <div className="mb-3">
                        <label htmlFor="usuario" className="form-label">Usuario</label>
                        <input type="text" className="form-control" id="usuario" value={usuario} onChange={(e) => {setUsuario(e.target.value)}}></input>
                    </div>
                    <div className="mb-3">
                        <label htmlFor="password" className="form-label">Password</label>
                        <input type="text" className="form-control" id="password" value={password} onChange={(e) => {setPassword(e.target.value)}}></input>
                    </div>
                    <div className="mb-3">
                        <label htmlFor="token" className="form-label">Token</label>
                        <input type="text" className="form-control" id="token" value={token} onChange={(e) => {setToken(e.target.value)}}></input>
                    </div>
                    <div className="mb-3">
                        <label htmlFor="roles" className="form-label">Roles</label>
                        <input type="text" className="form-control" id="roles" value={roles} onChange={(e) => {setRoles(e.target.value)}}></input>
                    </div>
                    <div className="mb-3">
                        <label htmlFor="activo" className="form-label">Activo</label>
                        <input type="text" className="form-control" id="activo" value={activo} onChange={(e) => {setActivo(e.target.value)}}></input>
                    </div>                
                    <div className="mb-12">
                        <button type="button" onClick={usuariosRegresar} className="btn btn-primary">Atras</button>
                        <button type="button" onClick={usuariosActualizar} className="btn btn-success">Actualizar</button>
                    </div>
                </div>
            </div>
        </div>
    )

}

//<div className="mb-3 form-check form-switch">
//<input className="form-check-input" type="checkbox" role="switch" id="activo"></input>
//</div>

export default UsuariosEditar;