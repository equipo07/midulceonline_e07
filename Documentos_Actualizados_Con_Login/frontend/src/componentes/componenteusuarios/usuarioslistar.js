import axios from 'axios';
import React, {useEffect, useState} from 'react';
import {Link} from 'react-router-dom';
import UsuariosBorrar from './usuariosborrar';

//Metodo que contiene las tareas para listar pedidos
function UsuariosListar()
{

    const[dataUsuarios, setdataUsuarios] = useState([])

    useEffect(()=>{
        axios.get('/api/usuarios/listar').then(res => {
        console.log(res.data)
        setdataUsuarios(res.data)
        }).catch(err=>{console.log(err.stack)})
    },[])
    
    return (
        <div className="container mt-5">
        <h1>Lista de Usuarios</h1>
        <div className="row">
            <div className="col-md-12">
                <table className="table table-bordered">
                    <thead className="thead-dark">
                        <tr key={0}>
                            <td align="center">Id</td>
                            <td>Correo</td>
                            <td>Usuario</td>
                            <td>Password</td>
                            <td>Token</td>
                            <td>Roles</td>
                            <td align="center">Estado</td>
                            {/*<td align="center"></td>*/}
                            <td colSpan={7} align="center"><Link to={`/usuariosagregar`}><li className='btn btn-success'>Agregar Usuario</li></Link></td>
                        </tr>
                    </thead> 
                    <tbody className='dialog'>
                    {
                        dataUsuarios.map(miusuario => (
                        <tr key={miusuario.id}>
                            <td align="center">{miusuario.id}</td>
                            <td>{miusuario.correo}</td>
                            <td>{miusuario.usuario}</td>
                            <td>{miusuario.password}</td>
                            <td>{miusuario.token}</td>
                            <td align="right">{miusuario.roles}</td>
                            <td align="center">{miusuario.activo ? 'Activo' : 'Inactivo'}</td>
                            <td align="center"><Link to={`/usuarioseditar/${miusuario.id}`}><li className='btn btn-info'>Editar</li></Link></td>
                            <td align="center"><li className='btn btn-danger' onClick={()=>{UsuariosBorrar(miusuario.id)}}>Borrar</li></td>
                        </tr>
                    ))
                    }
                    </tbody>
                </table>
            </div>
        </div>
        </div>
    )

}
export default UsuariosListar;

