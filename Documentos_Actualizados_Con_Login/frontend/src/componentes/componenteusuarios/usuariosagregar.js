import axios from 'axios';
import uniquid from 'uniqid';
import React, {useEffect, useState} from 'react';
import {useParams, useNavigate} from 'react-router';
import Swal from 'sweetalert2';

function UsuariosAgregar()
{
    const[correo, setCorreo] = useState('')
    const[usuario, setUsuario] = useState('')
    const[password, setPassword] = useState('')
    const[token, setToken] = useState('')
    const[roles, setRoles] = useState('')
    const[activo, setActivo] = useState('') 
    const navigate = useNavigate()
    
    function usuariosInsertar()
    {

        const usuarioinsertar = {
            id: uniquid(),
            correo: correo,
            usuario: usuario,
            password: password,
            token: token,
            roles: roles,
            activo: activo
        }

        console.log(usuarioinsertar)

        axios.post(`/api/usuarios/agregar`,usuarioinsertar).then(res => {
            console.log(res.data)
            Swal.fire({ position: 'center', icon: 'success',  title: 'El registro fue agregado exitosamente!', showConfirmButton: false, timer: 1500 })
            navigate('/usuarioslistar')
            })
            .catch(err => {console.log(err.stack)})
    
    }

    function usuariosRegresar()
    {
        //window.location.href="/";
        navigate('/usuarioslistar')
    }

    return(
        <div className="container mt-5">
            <h4>Nuevo Usuario</h4>
            <div className="row">
                <div className="col-md-12">
                <div className="mb-3">
                        <label htmlFor="correo" className="form-label">Correo</label>
                        <input type="text" className="form-control" id="correo" value={correo} onChange={(e) => {setCorreo(e.target.value)}}></input>
                    </div>                    
                    <div className="mb-3">
                        <label htmlFor="usuario" className="form-label">Usuario</label>
                        <input type="text" className="form-control" id="usuario" value={usuario} onChange={(e) => {setUsuario(e.target.value)}}></input>
                    </div>
                    <div className="mb-3">
                        <label htmlFor="password" className="form-label">Password</label>
                        <input type="text" className="form-control" id="password" value={password} onChange={(e) => {setPassword(e.target.value)}}></input>
                    </div>
                    <div className="mb-3">
                        <label htmlFor="token" className="form-label">Token</label>
                        <input type="text" className="form-control" id="token" value={token} onChange={(e) => {setToken(e.target.value)}}></input>
                    </div>
                    <div className="mb-3">
                        <label htmlFor="roles" className="form-label">Roles</label>
                        <input type="text" className="form-control" id="roles" value={roles} onChange={(e) => {setRoles(e.target.value)}}></input>
                    </div>
                    <div className="mb-3">
                        <label htmlFor="activo" className="form-label">Activo</label>
                        <input type="text" className="form-control" id="activo" value={activo} onChange={(e) => {setActivo(e.target.value)}}></input>
                    </div>                
                    <div className="mb-12">
                        <button type="button" onClick={usuariosRegresar} className="btn btn-primary">Atras</button>
                        <button type="button" onClick={usuariosInsertar} className="btn btn-success">Agregar</button>
                    </div>
                </div>
            </div>
        </div>
    )
}

export default UsuariosAgregar;