import axios from 'axios';
import React, {useEffect, useState} from 'react';
import {useParams, useNavigate} from 'react-router';
import Swal from 'sweetalert2';

function FacturasEditar()
{
    const parametros = useParams()
    
    const[carrito, setCarrito] = useState('')
    const[fecha, setFecha] = useState('')
    const[cliente, setCliente] = useState('')
    const[iva, setIva] = useState('')
    const[costo_total, setCosto_total] = useState('') 
    const navigate = useNavigate()
    
    useEffect(()=>{
        //axios.post('api/facturas/cargardata', {id: parametros.id}).then(res => {
        axios.get(`/api/facturas/cargar/${parametros.id}`).then(res => {
        //axios.post(`/api/facturas/cargardata/${parametros.id}`).then(res => {
        console.log(res.data[0])
        const dataFacturas = res.data[0]
        setCarrito(dataFacturas.carrito)
        setFecha(dataFacturas.fecha)
        setCliente(dataFacturas.cliente)
        setIva(dataFacturas.iva)
        setCosto_total(dataFacturas.costo_total)
        })
    }, [])

    function facturasActualizar()
    {
        const facturaactualizar = {
            id: parametros.id,
            carrito: carrito,
            fecha: fecha,
            cliente: cliente,
            iva: iva,
            costo_total: costo_total
        }

        console.log(facturaactualizar)

        axios.post(`/api/facturas/editar/${parametros.id}`,facturaactualizar).then(res => {
            console.log(res.data)
            Swal.fire({ position: 'center', icon: 'success',  title: 'El registro fue actualizado exitosamente!', showConfirmButton: false, timer: 1500 })
            navigate('/facturaslistar')
            })
            .catch(err => {console.log(err.stack)})
    
    }

    function facturasRegresar()
    {
        //window.location.href="/";
        navigate('/facturaslistar')
    }

    return(
        <div className="container mt-5">
            <h4>Factura</h4>
            <div className="row">
                <div className="col-md-12">
                <div className="mb-3">
                        <label htmlFor="carrito" className="form-label">Carrito</label>
                        <input type="text" className="form-control" id="carrito" value={carrito} onChange={(e) => {setCarrito(e.target.value)}}></input>
                    </div>                    
                    <div className="mb-3">
                        <label htmlFor="fecha" className="form-label">Fecha</label>
                        <input type="text" className="form-control" id="fecha" value={fecha} onChange={(e) => {setFecha(e.target.value)}}></input>
                    </div>
                    <div className="mb-3">
                        <label htmlFor="cliente" className="form-label">Cliente</label>
                        <input type="text" className="form-control" id="cliente" value={cliente} onChange={(e) => {setCliente(e.target.value)}}></input>
                    </div>
                    <div className="mb-3">
                        <label htmlFor="iva" className="form-label">Iva</label>
                        <input type="text" className="form-control" id="iva" value={iva} onChange={(e) => {setIva(e.target.value)}}></input>
                    </div>
                    <div className="mb-3">
                        <label htmlFor="costo_total" className="form-label">Costo total</label>
                        <input type="text" className="form-control" id="costo_total" value={costo_total} onChange={(e) => {setCosto_total(e.target.value)}}></input>
                    </div>                
                    <div className="mb-12">
                        <button type="button" onClick={facturasRegresar} className="btn btn-primary">Atras</button>
                        <button type="button" onClick={facturasActualizar} className="btn btn-success">Actualizar</button>
                    </div>
                </div>
            </div>
        </div>
    )

}


export default FacturasEditar;