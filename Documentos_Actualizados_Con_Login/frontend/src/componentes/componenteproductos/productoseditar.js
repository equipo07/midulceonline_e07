import axios from 'axios';
import React, {useEffect, useState} from 'react';
import {useParams, useNavigate} from 'react-router';
import Swal from 'sweetalert2';

function ProductosEditar()
{
    const parametros = useParams()
    
    const[nombre, setNombre] = useState('')
    const[categoria, setCategoria] = useState('')
    const[descripcion, setDescripcion] = useState('')
    const[precio, setPrecio] = useState('')
    const[activo, setActivo] = useState('') 
    const navigate = useNavigate()
    
    useEffect(()=>{
        //axios.post('api/pedidos/cargardata', {id: parametros.id}).then(res => {
        axios.get(`/api/productos/cargar/${parametros.id}`).then(res => {
        //axios.post(`/api/pedidos/cargardata/${parametros.id}`).then(res => {
        console.log(res.data[0])
        const dataProductos = res.data[0]
        setNombre(dataProductos.nombre)
        setCategoria(dataProductos.categoria)
        setDescripcion(dataProductos.descripcion)
        setPrecio(dataProductos.precio)
        setActivo(dataProductos.activo)
        })
    }, [])

    function productosActualizar()
    {
        const productoactualizar = {
            id: parametros.id,
            nombre: nombre,
            categoria: categoria,
            descripcion: descripcion,
            precio: precio,
            activo: activo
        }

        console.log(productoactualizar)

        axios.post(`/api/productos/editar/${parametros.id}`,productoactualizar).then(res => {
            console.log(res.data)
            Swal.fire({ position: 'center', icon: 'success',  title: 'El registro fue actualizado exitosamente!', showConfirmButton: false, timer: 1500 })
            navigate('/productoslistar')
            })
            .catch(err => {console.log(err.stack)})
    
    }

    function productosRegresar()
    {
        //window.location.href="/";
        navigate('/productoslistar')
    }

    return(
        <div className="container mt-5">
            <h4>Producto</h4>
            <div className="row">
                <div className="col-md-12">
                <div className="mb-3">
                        <label htmlFor="nombre" className="form-label">Nombre</label>
                        <input type="text" className="form-control" id="nombre" value={nombre} onChange={(e) => {setNombre(e.target.value)}}></input>
                    </div>                    
                    <div className="mb-3">
                        <label htmlFor="categoria" className="form-label">Categoria</label>
                        <input type="text" className="form-control" id="categoria" value={categoria} onChange={(e) => {setCategoria(e.target.value)}}></input>
                    </div>
                    <div className="mb-3">
                        <label htmlFor="descripcion" className="form-label">Descripcion</label>
                        <input type="text" className="form-control" id="descripcion" value={descripcion} onChange={(e) => {setDescripcion(e.target.value)}}></input>
                    </div>
                    <div className="mb-3">
                        <label htmlFor="precio" className="form-label">precio</label>
                        <input type="text" className="form-control" id="precio" value={precio} onChange={(e) => {setPrecio(e.target.value)}}></input>
                    </div>
                    <div className="mb-3">
                        <label htmlFor="activo" className="form-label">Activo</label>
                        <input type="text" className="form-control" id="activo" value={activo} onChange={(e) => {setActivo(e.target.value)}}></input>
                    </div>                
                    <div className="mb-12">
                        <button type="button" onClick={productosRegresar} className="btn btn-primary">Atras</button>
                        <button type="button" onClick={productosActualizar} className="btn btn-success">Actualizar</button>
                    </div>
                </div>
            </div>
        </div>
    )

}

//<div className="mb-3 form-check form-switch">
//<input className="form-check-input" type="checkbox" role="switch" id="activo"></input>
//</div>

export default ProductosEditar;