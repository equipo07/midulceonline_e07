import axios from 'axios';
import React, {useEffect, useState} from 'react';
import {Link} from 'react-router-dom';
import ProductosBorrar from './productosborrar';

//Metodo que contiene las tareas para listar pedidos
function ProductosListar()
{

    const[dataProductos, setdataProductos] = useState([])

    useEffect(()=>{
        axios.get('/api/productos/listar').then(res => {
        console.log(res.data)
        setdataProductos(res.data)
        }).catch(err=>{console.log(err.stack)})
    },[])
    
    return (
        <div className="container mt-5">
        <h1>Lista de Productos</h1>
        <div className="row">
            <div className="col-md-12">
                <table className="table table-bordered">
                    <thead className="thead-dark">
                        <tr key={0}>
                            <td align="center">Id</td>
                            <td>Nombre</td>
                            <td>Categoria</td>
                            <td>Descripcion</td>
                            <td>Precio</td>
                            <td align="center">Estado</td>
                            {/*<td align="center"></td>*/}
                            <td colSpan={7} align="center"><Link to={`/productosagregar`}><li className='btn btn-success'>Agregar Producto</li></Link></td>
                        </tr>
                    </thead> 
                    <tbody className='dialog'>
                    {
                        dataProductos.map(miproducto => (
                        <tr key={miproducto.id}>
                            <td align="center">{miproducto.id}</td>
                            <td>{miproducto.nombre}</td>
                            <td>{miproducto.categoria}</td>
                            <td>{miproducto.descripcion}</td>
                            <td align="right">{miproducto.precio}</td>
                            <td align="center">{miproducto.activo ? 'Activo' : 'Inactivo'}</td>
                            <td align="center"><Link to={`/productoseditar/${miproducto.id}`}><li className='btn btn-info'>Editar</li></Link></td>
                            <td align="center"><li className='btn btn-danger' onClick={()=>{ProductosBorrar(miproducto.id)}}>Borrar</li></td>
                        </tr>
                    ))
                    }
                    </tbody>
                </table>
            </div>
        </div>
        </div>
    )

}
export default ProductosListar;

