import Register from '../componentes/register';
import {Link} from 'react-router-dom';


import {BrowserRouter, Routes, Route} from 'react-router-dom';

function Login() {
  return (
     <div className="Login">
      <section className="login p-fixed d-flex text-center bg-primary common-img-bg">
	<div className="container-fluid">
		<div className="row">
			<div className="col-sm-12">
				<div className="login-card card-block">
					<form className="md-float-material">
						<div className="text-center">
							<img src="assets/images/mision.png" alt="logo" width="150" height="70"></img>
						</div>
						<h3 className="text-center txt-primary">
							Iniciar Sesión
						</h3>
						<div className="row">
							<div className="col-md-12">
								<div className="md-input-wrapper">
									<input type="email" className="md-form-control" required="required"></input>
									<label>Email</label>
								</div>
							</div>
							<div className="col-md-12">
								<div className="md-input-wrapper">
									<input type="password" className="md-form-control" required="required"></input>
									<label>Password</label>
								</div>
							</div>
							<div className="col-sm-6 col-xs-12">
							<div className="rkmd-checkbox checkbox-rotate checkbox-ripple m-b-25">
								<label className="input-checkbox checkbox-primary">
									<input type="checkbox" id="checkbox"></input>
									<span className="checkbox"></span>
								</label>
								<div className="captions">Rercordar Usuario</div>
							</div>
								</div>
							<div className="col-sm-6 col-xs-12 forgot-phone text-right">
								<a href="forgot-password.html" className="text-right f-w-600">Olvidaste el password?</a>
								</div>
						</div>
						<div className="row">
							<div className="col-xs-10 offset-xs-1">
								<button type="button" className="btn btn-primary btn-md btn-block waves-effect text-center m-b-20">Ingresar</button>
							</div>
						</div>
						<div className="card-footer">
						<div className="col-sm-12 col-xs-12 text-center">
							<span className="text-muted">No tienes cuenta?</span>
							{/*<Link to={"/register"} className="btn btn-block btn-danger">
                                    Crear Cuenta
  </Link>*/}


							<a href="/register" className="f-w-600 p-l-5">Registrate</a>
						</div>
						</div>
					</form>

				</div>

			</div>

		</div>

	</div>

</section>
<BrowserRouter>
      <Routes>
        <Route path='/register' element={<Register/>} exact></Route>     
      </Routes>    
    </BrowserRouter>

     </div>
  )
}

export default Login;